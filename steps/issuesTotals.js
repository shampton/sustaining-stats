const axios = require("axios").default;

/**
 * Get the total number of issues for a group
 * Get the issue statistics for a group for each label provided
 */

module.exports = config => {
  // adding an `undefined` label so we can get the stats for all issues in a group
  return Promise.all(
    [...config.labels, undefined].map(async label => {
      const labels = [
        config.groupLabel,
        label
      ].filter(item => item).join(",");

      const response = await axios({
        method: "get",
        url: `https://gitlab.com/api/v4/issues_statistics?scope=all&labels=${labels}`,
        headers: {
          "PRIVATE-TOKEN": config.gitlabPrivateToken
        }
      });

      return {
        label: label || "total",
        statistics: response.data.statistics.counts,
      };
    })
  );
};