const axios = require("axios").default;

/**
 * Get the statistics for number of issues completed each month and the number of engineers in the group that month
 */

module.exports = config => {
  return Promise.all(
    Object.entries(config.milestones).map(async ([title, value]) => {
      const result = await Promise.all(value.map(async assignee => {
        const response = await axios({
          method: "get",
          url: `https://gitlab.com/api/v4/issues_statistics?scope=all&milestone=${title}&assignee_username=${assignee}`,
          headers: {
            "PRIVATE-TOKEN": config.gitlabPrivateToken
          }
        });

        return response.data.statistics.counts.closed;
      }));

      const issuesClosed = result.reduce((a, b) => a + b, 0);

      return {
        milestone: title,
        issuesClosed,
        averagePerEngineer: issuesClosed / value.length,
      };
    })
  );
};