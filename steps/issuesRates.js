const axios = require("axios").default;

/**
 * Get the number of issues opened per milestone for the group
 * Get the number of issues opened per milestone with each label for the group
 */
module.exports = (config, milestones) => {
  // adding an `undefined` label so we can get the stats for all issues in a group
  return Promise.all([...config.labels, undefined].map(async label => {
    const milestoneIssues = await Promise.all(milestones.map(async milestone => {
      // adding an `undefined` label so we can get issues of any severity
      const severities = await Promise.all([...config.severityLabels, undefined].map(async severityLabel => {
        const labels = [config.groupLabel, label, severityLabel].filter(item => item).join(",");

        const response = await axios({
          method: "get",
          url: `https://gitlab.com/api/v4/issues_statistics?scope=all&labels=${labels}&created_before=${milestone.endDate}&created_after=${milestone.startDate}`,
          headers: {
            "PRIVATE-TOKEN": config.gitlabPrivateToken
          }
        });

        return {
          milestone,
          label: label || "total",
          severity: severityLabel || "any",
          statistics: response.data.statistics.counts,
        };
      }));

      return {
        milestone,
        severities,
      };
    }));

    return {
      label,
      milestones: milestoneIssues,
    };
  }));
};
