const fs = require("fs").promises;

/**
 * Output all of the statistics into a markdown file
 */

module.exports = (issuesTotals, issuesRates, completionRates) => {
  const finalOutput = ['# Statistics'];

  // Issue totals
  const totals = issuesTotals.map(total => {
    return `| ${total.label} | ${total.statistics.opened} |`
  });
  totals.unshift('| ----- | ----------- |');
  totals.unshift('| Label | Open Issues |');
  totals.unshift('');
  totals.unshift('## Issue totals');
  totals.unshift('');
  finalOutput.push(totals.join("\n"))

  // Issue rates
  const rates = issuesRates.map(label => {
    const sums = {};
    const labelOutput = label.milestones.map(milestone => {
      return milestone.severities.map(severity => {
        if (!sums[severity.severity]) {
          sums[severity.severity] = 0;
        }
        sums[severity.severity] += severity.statistics.all;

        return `| ${severity.milestone.title} | ${severity.label} | ${severity.severity} | ${severity.statistics.all} |`
      }).join("\n");
    });
    labelOutput.unshift('| --------- | ----- | -------- | -------------- |');
    labelOutput.unshift('| Milestone | Label | Severity | Issues created |');
    labelOutput.unshift('');
    labelOutput.unshift(`### ${label.label || "all"}`);
    labelOutput.unshift('');

    labelOutput.push('');
    Object.entries(sums).forEach(([title, value]) => {
      labelOutput.push(`Average ${title} issues opened per month: ${value / label.milestones.length}`)
    });

    return labelOutput.join("\n");
  });
  rates.unshift('## Issue rates');
  rates.unshift('');
  finalOutput.push(rates.join("\n"));

  // Completion rates
  let totalAverages = 0;
  const completion = completionRates.map(rate => {
    totalAverages += rate.averagePerEngineer;
    return `| ${rate.milestone} | ${rate.issuesClosed } | ${rate.averagePerEngineer} |`;
  });
  completion.unshift('| --------- | ------------- | -------------------- |');
  completion.unshift('| Milestone | Issues Closed | Average Per Engineer |');
  completion.unshift('');
  completion.unshift('## Completion rates');
  completion.unshift('');
  completion.push('');
  completion.push(`Average issues closed per engineer per month: ${totalAverages / completionRates.length}`);
  finalOutput.push(completion.join("\n"))

  // add extra newline at the bottom and write the file
  finalOutput.push('');
  fs.writeFile("./output.md", finalOutput.join("\n"));
};